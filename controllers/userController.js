const User = require('../models/userModel.js')
const _ = require('lodash')
const formidable = require('formidable')
const fs = require('fs')

exports.userById = (req, res, next, id)=>{
	User.findById(id)
	.populate('following',"_id name")
	.populate('followers',"_id name")
	.exec((err, user)=>{
		if(err || !user) {
			return res.status(400).json({
				error: "user not found"
			})
		}
		req.profile = user
		next()
	})
}

exports.recomended = (req, res)=>{
	let response=[]
	User.find((err, users)=>{
		if(err){
			res.status(400).json({error: err})
		}
		users.map((user) =>{
			if(user.followers.length > 1){
				response.push(user)
			}
		})
		res.send(response)
	})
}
exports.hasAuthorization = (req, res, next)=>{
	const authorized = req.profile && req.auth && req.profile._id == req.auth._id
	if(!authorized){
		return res.status(403).json({
			error: "User is not authorized to perform this action"
		})
	}
}

exports.allUsers = (req, res) =>{
	User.find((err, users)=>{
		if(err) {
			return res.status(400).json({error:err})
		}

		res.json(users)
	}).select("name email updated created imgPath")
}

exports.getUser = (req, res)=>{
	req.profile.hashed_password = undefined;
	req.profile.salt = undefined;
	return res.json(req.profile);
}


// exports.updateUser = (req, res, next)=>{
// 	console.log('im in')
// 	User.findByIdAndUpdate(req.profile._id, 
// 		{$set: { imgPath: req.body.selectedFile.filename,
// 				 name: req.body.name,
// 				 email: req.body.email,
// 				 created: Date.now() }},
// 				 {new: true}).then((docs)=>{
// 	 	if(!docs){
// 	 		console.log('no docs')
// 	 		res.status(400).json({message: "no such user exists"})
// 	 	}else{
// 	 		res.json({docs})
// 	 	}
// 	 }).catch((err)=>{
// 	 	res.status(400).json({error: err})
// 	 })
// }


exports.updateUser = (req, res, next)=>{
	let user = req.profile
	// console.log('user before update', user)
	console.log('req.file is ', req.file)
	console.log('request body',req.body)
	user = _.extend(user, req.body)
	user.updated = Date.now()
	user.imgPath = req.file.filename
	// console.log('user after update', user)
	user.save((err, user)=>{
		if(err){
			res.status(400).json({error: err})
		}
		res.json({user})
	})
}
// exports.updateUser = (req, res, next)=>{
// 	let form = new  formidable.IncomingForm()
// 	form.keepExtensions = true;
// 	form.parse(req, (err, fields, files)=>{
// 		if(err){
// 			console.log(err)
// 			return res.status(400).json({
// 				error: "Photo could not be uploaded"
// 			})
// 		}

// 		//save user
// 		let user = req.profile
// 		user = _.extend(user, fields)
// 		user.updated = Date.now()

// 		if(files.photo){
// 			user.photo.date = fs.readFileSync(files.photo.path)
// 			user.photo.contentType = files.photo.type
// 		}
// 		user.save((err, result)=>{
// 			if(err){
// 				return(res.status(400).json({error: err}))
// 			}
// 			user.hashed_password = undefined;
// 			user.salt = undefined;
// 			res.json(user)
// 		});
// 	});
// };
// exports.updateUser = ()=>{
// 	let user = req.profile;
// 	user = _.extend(user, req.body)
// 	user.updated = Date.now()
// 	user.save((err)=>{
// 		if(err){
// 			return res.status(400).json({error: err})
// 		}
// 		res.sen(user)
// 	})
// }

exports.userPhoto = (req, res, next)=>{
	if(req.profile.photo.data){
		res.set("Content-Type",req.profile.photo.contentType);
		return(res.send(req.profile.photo.data))
	}
	next()
}


exports.deleteUser = (req, res, next)=>{
	let user = req.profile;
	user.remove((err, user)=>{
		if(err){
			user.hashed_password = undefined;
			user.salt = undefined;
			return res.status(400).json({error: err})
		}
		res.json({message: "user deleted successfully"})
	})
}

exports.addFollowing = (req, res, next)=>{
	User.findByIdAndUpdate(
		req.body.userId,
		{$push: {following: req.body.followId}},
		(err, result)=>{
		if(err){
			return res.status(400).json({error: err})
		}
		console.log('succsess in addFollowing')
		next()
	})
}

exports.addFollower = (req, res)=>{
	User.findByIdAndUpdate(
		req.body.followId,
		{$push: {followers: req.body.userId}},
		{new: true}
		)
		.populate('following', '_id name')
		.populate('followers', '_id name')
		.exec((err,result)=>{
			if(err){
				res.status(400).json({error: err})
			}
			result.hashed_password = undefined;
			result.salt = undefined
			res.json(result)
		})
};


exports.removeFollowing = (req, res, next)=>{
	User.findByIdAndUpdate(
		req.body.userId,
		{$pull: {following: req.body.unfollowId}},
		(err, result)=>{
		if(err){
			return res.status(400).json({error: err})
		}
		next()
	})
}

exports.removeFollower = (req, res)=>{
	User.findByIdAndUpdate(
		req.body.unfollowId,
		{$pull: {followers: req.body.userId}},
		{new: true}
		)
		.populate('following', '_id name')
		.populate('followers', '_id name')
		.exec((err,result)=>{
			if(err){
				res.status(400).json({error: err})
			}
			result.hashed_password = undefined;
			result.salt = undefined
			res.send(result)
		})
};
