const Post = require('../models/postModel.js')
const formidable = require('formidable')
const fs = require('fs')
const _ = require('lodash')

//file upload system





exports.postById = (req, res, next, id)=>{
	Post.findById(id)
	.populate("postedBy", "_id name")
	.exec((err, post)=>{
		if(err || !post){
			return res.status(400).json({error: err})
		}
		req.post = post;
		next()
	})
}
exports.createPost = (req, res)=>{
	var post = new Post({
		title: req.body.title,
		body: req.body.bodyText,
		imgPath: req.file.filename,
		created: Date.now()
	})
	post.postedBy = req.profile;

	post.save((error, result)=>{
		if(error){
			return res.status(400).json({error})
		}
		console.log(result)
		res.json({message: "post saved successfully",
				 post: result})
	})
}
// exports.createPost = (req, res, next)=>{
// 	let form = new formidable.IncomingForm()
// 	form.keepExtensions = true;
// 	form.parse(req, (err, fields, files)=>{
// 		if(err) {
// 			console.log(err);
// 			return res.status(400).json({
// 				error: "Image could not be uploaded"
// 			})
// 		}

// 		let post = new Post(fields)
// 		post.postedBy = req.profile
// 		if(files.photo){
// 			post.photo.data = fs.readFileSync(files.photo.path)
// 			post.photo.contentType = files.photo.type
// 		}

// 		post.save((err, result)=>{
// 			if(err){
// 				return res.status(400).json({
// 					error: err + `  error while saving the post`
// 				})
// 			}
// 			req.profile.hashed_password = undefined;
// 			req.profile.salt = undefined;
// 			res.json(result)
// 		})
// 	})
// }

// exports.createPost = (req, res, next)=>{
	// console.log('begining')
	// var img = fs.readFileSync(req.file.path);
 // var encode_image = img.toString('base64');
 // // Define a JSONobject for the image attributes for saving to database
 
 // var finalImg = {
 //      contentType: req.file.mimetype,
 //      image:  new Buffer(encode_image, 'base64')
 //   };

 //   let post= req.body;
 //   post.postedBy = req.profile;
 //   post.filename = finalImg
 //   console.log('sanam sheinaxavs posti', post)
 //   post.save((err,result)=>{
 //   	if(err){
 //   		console.log('error in file uploads is', err)
 //   		return res.status(400).json({
 //   			error: err
 //   		})

 //   		res.send(result)
 //   	}
 //   })
// }

exports.getPost = (req, res)=>{
	const posts = Post.find()
	.populate("postedBy", "_id name")
	.populate("comments", "text createPost")
	.populate("comments.postedBy","_id name" )
	.select("_id imgPath title body created likes")
	.sort({created: -1})
	.then(posts =>{
		console.log(posts)
		res.json(posts)
	})
	.catch(err =>console.log(err))
}

exports.postByUser = (req,res)=>{
	Post.find({postedBy: req.profile._id})
	.populate("postedBy", "_id name")
	.select("_id title body created likes")
	.sort("_created")
	.exec((err, posts)=>{
		if(err){
			return res.status(400).json({
				errorType: "PostByUser Error",
				error: err
			})
		}
		res.json(posts)
	})
};

exports.isPoster = (req, res, next) =>{
	let isPoster = req.post && req.auth && req.post.postedBy._id == req.auth._id
	if(!isPoster){
		return (res.status(403).json({
			error: "user is not authorized"
		}))
	}
	next(  )

}

exports.deletePost = (req, res) => {
	let post = req.post
	post.remove((err, post) =>{
		if(err){
			res.status(400).json({
				errorType: "delete user error",
				error: err
			})
		}
		res.json({
			message: "post deleted successfully"
		})
	})
}

exports.updatePost = (req, res, next)=>{
	let form = new formidable.IncomingForm();
	form.keepExtension = true;
	form.parse(req,(err, fields,files)=>{
		if(err){
			res.status(400).json({error: "Photo couldn't be uploaded"})
		}

		let post = req.post;
		post = _.extend(post, fields)
		post.updated = Date.now

		if(files.photo){
			post.photo.data =fs.readFileSync(files.photo.path)
			post.photo.contentType = files.photo.type
		}
		post.save((err, result)=>{
			if(err){
				return res.status(400).json({error: err})
			}
			res.json(post)
		});

	});

};

exports.photo = (req, res, next)=>{
	res.set("content-Type", req.post.photo.contentType)
	return res.send(req.post.photo.date)
}

exports.singlePost = (req,res)=>{
	return res.json(req.post)
}

exports.like = (req, res)=>{
	console.log('req.boy in like request',req.body)
	Post.findByIdAndUpdate(req.body.postId, 
		{$push:{likes:	req.body.userId}},
		{new: true}
).exec((err, result)=>{
	if(err){
		res.status(400).json({errorFrom: "error from like requiest",
							  error: err})
	}
	console.log(result)
	res.json(result);
})
}

exports.unlike = (req, res)=>{
	Post.findByIdAndUpdate(req.body.postId, {$pull:{likes:	req.body.userId}},
	{new: true}
).exec((err, result)=>{
	if(err){
		res.status(400).json({error: err})
	}
	res.json(result);
})
}

exports.comment = (req, res)=>{
	let comment = req.body.comment
	comment.postedBy = req.body.userId
	Post.findByIdAndUpdate(req.body.postId,
		{$push: {comments: comment}},
		{new: true})
	.populate("comments.postedBy", "_id name")
	.populate("comments", "_id name")
	.exec((err, result)=>{
		if(err) {
			return res.status(400).json({error: err})
		}
		res.json(result)
	});
};


exports.uncomment = (req, res)=>{
	let comment = req.body.comment

	Post.findByIdAndUpdate(req.body.postId,
		{$pull: {comments: {_id: comment._id}}},
		{new: true}
	)

	.populate("comments.postedBy", "_id name")
	.populate("comments", "_id name")
	.exec((err, result)=>{
		if(err) {
			return res.status(400).json({error: err})
		}
		res.json(result)
	});
};
