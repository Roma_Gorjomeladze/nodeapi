const User = require('../models/userModel.js')
const jwt = require('jsonwebtoken')
const expressJwt = require('express-jwt')
require('dotenv').config()

exports.signup = async (req, res)=>{
	const userExists = await User.findOne({email: req.body.email})
	if(userExists)

		return res.status(203).json({
			error: "email is taken"
		})
	const user = await new User(req.body)
	await user.save()
	res.status(200).json({
	message: "signup success, you can login"
	})
}

exports.signin = (req, res)=>{
	//find the user based on email
	const { email, password} = req.body
	User.findOne({email}, (err, user)=>{

	//if error or no user

		if(err || !user) {
			return res.status(401).json({
					error: "User with that email doesn't exists please signUp"
			 })
		}
		//if user is found make sure that email and pasword matches
		//create authenticate method and use here

		if(!user.authenticate(password)) {
			return res.status(401).json({
				error: "Email and password don't matches"
			})
		}

		//generate a token with user id  and secret
		let token = jwt.sign({_id: user._id}, process.env.JWT_SECRET)

		//porsist the token as 't' in the cookie with expire date
		res.cookie("t", token, {expire: new Date() + 9999});
		const {_id, name, email} = user
		//return response with user and token  to frontend client
		return res.json({token, user: {_id, email, name}})
	});
}

exports.signout = (req, res)=>{
	res.clearCookie("t")
	return res.json({message: "Signout success!"})
}

exports.requireSignin = expressJwt({
	//if the token is valid, express jwt appends the verified user id
	//in an auth key to the request object
	secret: process.env.JWT_SECRET,
	userProperty: "auth"
})
