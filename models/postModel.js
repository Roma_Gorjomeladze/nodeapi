const mongoose = require('mongoose')
const { ObjectId } = mongoose.Schema
const PostSChema = new mongoose.Schema({
	title: {
		type: String,
		require: true,
	},
	body: {
		type: String,
		require: true,
	},
	photo: {
		type: Buffer,
		contentType: String
	},
	postedBy: {
		type: ObjectId,
		ref: "User"
	},
	created: {
		type: Date,
		default: Date.now
	},
	imgPath: {
		type: String,
		require: false
	},
	likes: [{type: ObjectId, ref: "User"}],
	comments: [
		{
			text: String,
			created: {type: Date, default: Date.now},
			postedBy: {type: ObjectId, ref: "User"}
		}]
});

module.exports = mongoose.model("Post", PostSChema)
