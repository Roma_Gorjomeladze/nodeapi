const mongoose = require('mongoose')
const uuidv1 = require('uuid/v1')
const { ObjectId } = mongoose.Schema
const crypto = require('crypto')
const UserSchema = mongoose.Schema({
	name: {
		type: String,
		require: true,
		trim: true
	},
	email: {
		type: String,
		require: true,
		trim: true
	},
	hashed_password: {
		type: String,
		require: true
	},
	salt: String,
	created: {
		type: Date,
		default: Date.now
	},
	updated: Date,
	imgPath: {
		type: String,
		require: false
	},
	about: {
		type: String,
		trim: true
	},
	following: [{type: ObjectId, ref: "User"}],
	followers: [{type: ObjectId, ref: "User"}]
})

UserSchema
.virtual('password')
.set(function(password){

	//create temporary variable _password
	this._password = password

	//generate the timestamp
	this.salt = uuidv1()

	//encript password
	this.hashed_password = this.encryptPassword(password)
})
.get(function(){
	return this._password
})

// METHODS

UserSchema.methods = {
	authenticate: function(plainText) {
		return this.encryptPassword(plainText) === this.hashed_password
	},
	encryptPassword: function(password){
		if(!password) return ""
			try{
				return crypto.createHmac('sha1',this.salt)
					.update(password)
					.digest('hex');
			}catch (err){
					return ""
			}
	}
}


module.exports = mongoose.model("User", UserSchema )
