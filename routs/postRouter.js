//file upload system
const uuidv1 = require('uuid/v1')
const path= require('path')
var multer  = require('multer')
const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, './uploads')
  },
  filename: (req,file,cb)=>{
    const newFilename = `${uuidv1()}${path.extname(file.originalname)}`;
    cb(null, newFilename);
  }
})
const upload = multer({storage})

const router = require('express').Router()
const { postByUser,
	 			getPost,
				createPost,
				postById,
				deletePost,
				isPoster,
				updatePost,
			 	like,
				unlike,
				singlePost,
				photo,
				comment,
				uncomment,
				test} = require('../controllers/postController.js')
const { requireSignin } = require('../controllers/authController.js')
const { createPostValidator } = require('../validator/index.js')
const { userById } = require('../controllers/userController.js')

router.post(
	'/post/new/:userId',
	requireSignin,
	upload.single('selectedFile'),
	createPost)

router.get('/posts', getPost)
//likes
router.put('post/like', requireSignin, like)
router.put('post/unlike', requireSignin, unlike)
//comments
router.put('post/comment', requireSignin, comment)
router.put('post/uncomment', requireSignin, uncomment)


router.get('/post/by/:userId', requireSignin, postByUser)
router.get('/post/:postId', singlePost)
router.delete('/post/:postId', requireSignin, isPoster, deletePost)
router.put('/post/:postId', requireSignin, isPoster, updatePost)

router.get('/post/photo/:postId', photo)
//any route containing :userId, our app will first execute userById

router.param("userId", userById)
router.param("postId", postById)

module.exports = router
