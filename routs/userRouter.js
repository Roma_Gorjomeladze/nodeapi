const router = require('express').Router()
var multer  = require('multer')
const uuidv1 = require('uuid/v1')
const path= require('path')
const storage = multer.diskStorage({
  destination: (req, file, cb)=>{
    cb(null, './uploads')
  },
  filename: (req,file,cb)=>{
    const newFilename = `${uuidv1()}${path.extname(file.originalname)}`;
    cb(null, newFilename);
  }
})
const upload = multer({storage})

const { userById,
        deleteUser,
        updateUser,
        allUsers,
        getUser,
        userPhoto,
        addFollowing,
        addFollower,
        removeFollower,
        removeFollowing,
        recomended
      } = require('../controllers/userController.js')
const { requireSignin } = require('../controllers/authController.js')

router.put('/user/follow', requireSignin, addFollowing, addFollower)
router.put('/user/unfollow', requireSignin, removeFollowing, removeFollower)

router.get('/users', allUsers)
router.get('/recomended', recomended)
router.get('/user/:userId', requireSignin, getUser)
router.put('/user/:userId', requireSignin, upload.single('selectedFile'), updateUser)
router.delete('/user/:userId', requireSignin, deleteUser)

router.get('/user/photo/:userId', userPhoto);

//any route containing :userId, our app will first execute userById
router.param("userId", userById)

module.exports = router
