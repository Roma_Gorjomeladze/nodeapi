require('dotenv').config()
const express = require('express')
const bodyParser = require('body-parser')
const uuidv1 = require('uuid/v1')
const app = express()
const postRouter = require('./routs/postRouter.js')
const authRouts = require('./routs/authRouter.js')
const userRouter = require('./routs/userRouter.js')
const PORT = process.env.PORT
const URI = process.env.URI
const multer = require('multer')
const morgan = require('morgan')
var cookieParser = require('cookie-parser')
const mongoose = require('mongoose')
const fs = require('fs') 
const path = require('path')
const cors = require('cors')
const expressValidator = require('express-validator')



mongoose.connect(URI, {useNewUrlParser: true})
.then(()=>console.log('connected to mogoose'))

mongoose.connection.on('error', err => console.log(`Db Connection error: ${err.message}`))
app.use(express.json())
app.use(express.static(path.join(__dirname, '/uploads')))
app.use(cookieParser())
app.use(morgan('dev'))
app.use(expressValidator())
app.use(cors())
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.use('/', postRouter)
app.use('/', authRouts)
app.use('/',userRouter)

//apiDocs


app.get('/', (req,res)=>{
	fs.readFile('docs/apiDocs.json', (err, data)=>{
		if(err){
			res.status(400).json({
				error: err
			})
		}
		let docs = JSON.parse(data)
		res.json(docs)
	})
})
app.use(function (err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401).json({error: "unauthorized user"});
  }
});

app.listen(PORT, ()=>{	
	console.log(`app listens port ${PORT}`)
})	